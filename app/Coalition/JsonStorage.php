<?php

namespace App\Coalition;

class JsonStorage {

    const STORAGE_FILENAME = 'products.json';

    protected static $data;

    protected static $nextId;

    public static function storeNewProduct(array $data = [])
    {
        self::getStoredData();
        $data = array_merge(
            $data,
            [
                'id' => self::$nextId,
                'submitted_at' => now()
            ]
        );

        self::addToStoredData($data);
        self::saveStoredData();

        return $data;
    }

    public static function addToStoredData(array $data = [])
    {
        self::$data[] = $data;
        self::$nextId++;
    }

    public static function getStoredData()
    {
        self::$data = json_decode(file_get_contents(storage_path() . DIRECTORY_SEPARATOR . self::STORAGE_FILENAME));
        self::$nextId = count(self::$data) + 1;

        return self::$data;
    }

    public static function saveStoredData()
    {
        file_put_contents(storage_path() . DIRECTORY_SEPARATOR . self::STORAGE_FILENAME, json_encode(self::$data));
    }

    public static function updateProduct(int $productId, array $data = [])
    {
        self::getStoredData();
        foreach (self::$data as &$dataItem) {
            if ($dataItem->id === $productId) {
                $newItem = array_merge((array)$dataItem, $data);
                $dataItem = $newItem;
            }
        }
        self::saveStoredData();

        return $newItem;
    }
}