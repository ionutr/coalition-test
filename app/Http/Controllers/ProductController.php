<?php

namespace App\Http\Controllers;

use App\Coalition\JsonStorage;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return response()->json(JsonStorage::getStoredData());
    }

    public function store(ProductRequest $request)
    {
        return response()->json(JsonStorage::storeNewProduct($request->all()), 201);
    }

    public function update(ProductRequest $request, $productId)
    {
        return response()->json(JsonStorage::updateProduct($productId, $request->all()), 200);
    }
}
