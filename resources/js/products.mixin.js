export default {
    methods: {
        createProduct(data) {
            let request = window.axios.post('products', data, {
                withCredentials: false
            })

            return request
                    .then(result => { return result })
                    .catch(error => { throw error })
        },
        getProducts() {
            let request = window.axios.get('products', {
                withCredentials: false
            })

            return request
                    .then(result => { return result })
                    .catch(error => { throw error })
        },
        updateProduct(id, data) {
            let request = window.axios.patch(`products/${id}`, data, {
                withCredentials: false
            })

            return request
                    .then(result => { return result })
                    .catch(error => { throw error })
        },
    },
    name: 'ProductsMixin'
}